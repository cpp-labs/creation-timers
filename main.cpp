#include <iostream>
#include <chrono>
#include <array>
#include <numeric>
#include <algorithm>

using std::array;
using std::iota;
using std::cout;
using std::endl;
using std::sort;

class Timer {
private:
    // Type aliases to make accessing nested type easier
    using clock_type = std::chrono::steady_clock;
    using second_type = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_type> m_beg{ clock_type::now() };

public:

    void reset()
    {
        m_beg = clock_type::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_type>(clock_type::now() - m_beg).count();
    }
};

struct Sample {
    int x;
    int y;
};

struct AnotherSample {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
};

int main() {
    const int array_length = 10000;


    Timer t1;
    array<int, array_length> arr1;
    iota(arr1.rbegin(), arr1.rend(), 1); // fill the array with values 10000 to 1
    sort(arr1.begin(), arr1.end());
    cout << "Time taken to make 10.000 elements int array: " << t1.elapsed() << endl;

    Timer t2;
    array<int, array_length * 10> arr2;
    iota(arr2.rbegin(), arr2.rend(), 1); // fill the array with values 10000 to 1
    sort(arr2.begin(), arr2.end());
    cout << "Time taken to make and sort 100.000 elements int array: " << t2.elapsed() << endl << endl;

    Timer t3;
    Sample arr3[array_length];
    for (Sample &s : arr3) {s = {1, 2};}
    cout << "Time taken to make Sample{int,int} struct array with 10.000 elements " << t3.elapsed() << endl;

    Timer t4;
    AnotherSample arr4[array_length*5];
    for (AnotherSample &a : arr4) {a = {1, 2, 3, 4, 5, 7};}
    cout << "Time taken to make AnotherSample{int,int,int,int,int,int,int} struct array with 50.000 elements "
         << t4.elapsed() << endl;

    return 0;
}
